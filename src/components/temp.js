import React , {Component} from 'react';
import {
    Card, CardImg, CardText, CardBody,
    CardTitle,  Row, Col, Label, ListGroup, ListGroupItem, ListGroupItemHeading, ListGroupItemText,  Breadcrumb, BreadcrumbItem , Modal, ModalHeader, ModalBody, Button
} from 'reactstrap';
import { Link } from 'react-router-dom';

import Rating from 'react-rating';

import { Control, LocalForm, Errors } from 'react-redux-form';

const required = (val) => val && val.length;
const maxLength = (len) => (val) => !(val) || (val.length <= len);
const minLength = (len) => (val) => val && (val.length >= len);

class CommentForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rating: 1,
            name: '',
            comment: '',
            isModalOpen: false,
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
    }
    handleSubmit(values) {
        this.toggleModal();
        this.props.addComment(this.props.dishId, values.rating, values.author, values.comment);
    }
    toggleModal() {
        this.setState({
          isModalOpen: !this.state.isModalOpen
        });
      }

    render() {
        return (
            <div>
                <Button outline color="secondary" onClick={this.toggleModal} className='mt-5'>
                <span className="fa fa-pencil"></span>
                    Submit Comment
                </Button>
                <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
                <ModalHeader toggle={this.toggleModal}>Submit Comment</ModalHeader>
                <ModalBody>
                    <LocalForm onSubmit={(values) => this.handleSubmit(values)}>
                        <Row>
                            <Label md={12} htmlFor="rating">Rating</Label>
                            <Col>
                                <Control.select
                                    model=".rating"
                                    name="rating"
                                    className="form-control">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                </Control.select>
                            </Col>
                        </Row>
                        <Row>
                            <Label md={12} htmlFor="author">Your Name</Label>
                            <Col>
                                <Control.text
                                    model=".author"
                                    id="author"
                                    name="author"
                                    placeholder="Your Name"
                                    className="form-control"
                                    validators={{
                                        required, minLength: minLength(3), maxLength: maxLength(15)
                                    }}
                                />
                                <Errors
                                    className="text-danger"
                                    model=".author"
                                    show="touched"
                                    messages={{
                                        required: 'Required',
                                        minLength: 'Must be greater than 2 characters',
                                        maxLength: 'Must be 15 characters or less'
                                    }}
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Label md={12} htmlFor="comment">Comment</Label>
                            <Col>
                                <Control.textarea
                                    model=".comment"
                                    id="comment"
                                    name="comment"
                                    placeholder="Comment"
                                    className="form-control"
                                    validators={{
                                        required, minLength: minLength(3), maxLength: maxLength(15)
                                    }}
                                />
                            </Col>
                        </Row>
                        <Row >
                            <Col>
                                <Button type="submit" color="primary" className='mt-5'>
                                    Submit
                    </Button>
                            </Col>
                        </Row>
                    </LocalForm>
                </ModalBody>
            </Modal>
            </div>
        )
    }
}

function RenderDish({ dish }) {
    return (
        <Col md={5} className="m-1">
            <Card key={dish.id}>
                <CardImg top src={dish.image} alt={dish.name} />
                <CardBody>
                    <CardTitle>{dish.name}</CardTitle>
                    <CardText>{dish.description}
                    </CardText>
                </CardBody>
            </Card>
        </Col>
    )
}

function RenderComments({ comments ,addComment,dishId}) {

    return (

        <Col md={5} className="m-1">
            <ListGroup>
                <ListGroupItemHeading><h4>User Comments</h4></ListGroupItemHeading>
                {comments.map((comment) => {
                    return (
                        <ListGroupItem key={comment.author}>
                            <ListGroupItemText>
                                <strong>User:</strong>{comment.author}
                            </ListGroupItemText>
                            <ListGroupItemText>
                                {comment.comment}
                            </ListGroupItemText>
                            <ListGroupItemText>
                                <small>{(comment.date).format("MMM-DD-YYYY")}</small>
                            </ListGroupItemText>
                            <ListGroupItemText>
                                <strong>Rating:</strong> <br />
                                <Rating initialRating={comment.rating} />
                            </ListGroupItemText>
                        </ListGroupItem>
                    )
                })}
            </ListGroup>
            <CommentForm  dishId={dishId} addComment={addComment}/>
        </Col>

    )
}

const DishDetail = (props) => {
    return (
        <div className="container">
            <div className="row">
                    <Breadcrumb>
                        <BreadcrumbItem><Link to="/menu">Menu</Link></BreadcrumbItem>
                        <BreadcrumbItem active></BreadcrumbItem>
                    </Breadcrumb>
                    <div className="col-12">
                        <h3>{props.dishDetail.name}</h3>
                        <hr />
                    </div>                
                </div>
            {props.dishDetail !== undefined && (
                <div className="row">
                    <RenderDish dish={props.dishDetail} />

                    <RenderComments comments={props.comments}
                    addComment={props.addComment}
                    dishId={props.dish.id} />
                </div>
            )}

        </div>
    );
}

export default DishDetail;